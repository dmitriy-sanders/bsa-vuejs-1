<<<<<<< HEAD
=======
# About

SPA for managing users, using Vue.js, axios.

## Features:
- Add User
- Delete User
- Edit User
- Search User (with filters or not)

## Installing
- git clone https://dmitriy-sanders@bitbucket.org/dmitriy-sanders/bsa-vuejs-1.git
- cd bsa-vuejs-1
- docker-compose up -d
- docker-compose run —-rm composer composer install
- cp .env.example .env
- docker-compose exec web php artisan key:generate
- docker-compose exec web php artisan storage:link
- sudo chmod -R 777 storage/
- docker-compose exec web php artisan migrate
- docker-compose run --rm node npm install
- docker-compose run --rm node npm run dev
- docker-compose exec web php artisan db:seed (Will be generated 10 users)

App will be available at http://127.0.0.1:8095
